<?php
/* 
 * Requires a file that is NOT to be put under version control: creds.php.
 * That file is the server address and credentials for database access.
 * Just ask for the credentials and make the file, keep only a private copy.
 *
 * This will be required in each file. Might not do much more than connect to 
 * database, as far as functionality goes.
 */

require_once 'creds.php';

$conn = mysql_connect ($db_address, $username, $passwd) or 
		die ('Failed to connect to database at ' . $db_address);
?>