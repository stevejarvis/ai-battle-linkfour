<head>
<style TYPE="text/css">
	body {
		background-color:#cccccc;
	}
	h1 {
		color:blue;
		text-align:center;
		margin:auto;
	}
	p {
		font-size:20px;
	}
	footer {
		text-align:center;
		position: absolute;
		bottom: 0px;
		color:blue;
		left: 0px;
	    width: 100%;
	}
	table {
		margin: 4em;
	}
	td {
		margin: 1em;
	}
</style>
</head>

<!--
 * Main is the skeleton for the page. It defines the setup and just calls the other
 * files when/where they're needed.
-->

<body>
<?php //require_once 'header.php'; ?>
<h1>Link Four battle for a free trip to Portland*!</h1>

<table width="100%">
	<tr>
		<td width="25%"><?php include 'current_standings.php'; ?></td>
		<td width="75%">Board Here<BR>Updated <?php date_default_timezone_set('America/Los_Angeles'); 
				$now = localtime(time(), true);
				echo $now['tm_hour'] . ':' . $now['tm_min'] . ':' . $now['tm_sec'];?></td>
	</tr>
</table>
<footer><?php include 'footer.php';?></footer>
</body>
