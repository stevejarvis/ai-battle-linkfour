<?php
/* 
 * Keeping all the queries here. Makes editing/reusing easy.
 */

$query_get_most_recent_teams = 'SELECT red_team, black_team
								FROM games
								ORDER BY time DESC
								LIMIT 1';

$query_get_record_ordered = 'SELECT team_name, win, loss, tie, SUM(win + tie + loss) AS "total"
							 FROM teams
							 ORDER BY total DESC';

$query_get_most_recent_boards = 'SELECT play
								 FROM games
								 ORDER BY time DESC
								 LIMIT 1';
?>
