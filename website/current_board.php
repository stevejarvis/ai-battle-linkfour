<?php
/*
 * Get the current board from the database and write it to the screen.
 */

// Returns latest board as a string of 42 characters.
function getLatestBoard() {
	require_once ('queries.php');
	$res = mysql_query($query_get_most_recent_boards);
	$row = mysql_fetch_assoc($res);
	$all_boards = explode('\n', $row['play'], -1);
	$current = $all_boards[count($all_boards) - 1];
	return $current;
}

// Nice to have a nice title on top of the board, tell who's playing.
function outputCurrentGameTitle () {
	require_once ('queries.php');
	$res = mysql_query ($query_get_most_recent_teams) or 
			die ('Could not query for the most recent teams.');
	$row = mysql_fetch_assoc ($res); 
	echo $row['red_team'] . ' as red VS ' . $row['black_team'] . ' as black';
}

function outputToScreen($board) {
	echo '<table>';
	$pos = 0;
	for($j = 0; $j < 6; $j++) {
		echo '<tr>'
		for($i = 0; $i < 7; $j++) {
			echo "<td>|" . $board[$pos++] . "</td>";
		}
		echo "|</tr>";
	}
	echo '</table>'
}

$cur_board = getLatestBoard();
outputCurrentGameTitle();
outputToScreen ($cur_board);
?>
