<?php
/*
 * Gets all the current teams and point standings. Orders them for most points.
 * Then prints them in a table.
 */

require_once 'header.php';

// Returns a sorted (by total) array of <team_name> => array(win,tie,loss,total)
function getCurrentStandings () {
	require_once ('queries.php');
	$res = mysql_query ($query_get_record_ordered) or 
			die ('Could not query database for points.');
	while ($row = mysql_fetch_assoc ($res)) {
		$stands[$row['team_name']] = array ('win' => $row['win'],
										   'tie' => $row['tie'],
										   'loss' => $row['loss'],
										   'total' => $row['total']);
	}

	return $stands;
}

// Print the results, as assembled by getCurrentStandings, to screen.
function outputResults ($standings) {
	echo '<table>
			<tr>
				<td>Team Name</td>
				<td>Wins</td>
				<td>Ties</td>
				<td>Losses</td>
				<td>Total Points</td>
			</tr>';

	foreach ($standings as $team => $stats) {
		echo "<tr>
				<td>$team</td>
				<td>" . $stats['win'] . "</td>
				<td>" . $stats['tie'] . "</td>
				<td>" . $stats['loss'] . "</td>
				<td>" . $stats['total'] . "</td>
			</tr>";
	}
	 echo '</table>';
}

$standings = getCurrentStandings();
outputResults ($standings);
?>
