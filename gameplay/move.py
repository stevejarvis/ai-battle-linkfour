'''
Created on Jan 30, 2012

@author: steve

'''

class Move(object):
    '''
    Keeps information about each move. Useful for getting coords
    of plays.
    '''
    
    def __init__(self, row, col, team):
        self.col = col
        self.row = row
        self.team_color = team
        
    def get_coordinates(self):
        return (self.row, self.col)
    
    def get_team(self):
        return self.team_color