'''
Created on Jan 28, 2012

@author: steve

'''

from error import *
from move import Move
from utility import EmptySpace, Logger
import sys

class Board(object):
    '''
    The board is a 2D array, 6 rows 7 columns. All spaces initialized
    to BlankSpace object (in utility file), if there's a play it will 
    simply be marked r/b (red or black). Plays also tracked in list for
    easier question answering.
    
    Referenced board[row][column]
    '''

    def __init__(self):
        # List of Moves... of all the moves.
        self.moves = []
        # Double array on hand to represent as well.
        self.board = []
        for row in range(0, 6):
            self.board.append([])
            for column in range(0, 7):
                self.board[row].append(EmptySpace())
    
    def __repr__(self):
        representation = ""
        for row in range(0, 6):
            for column in range(0, 7):
                representation += str(self.board[row][column])
        return representation
     
    # Figure out where the move belongs.        
    def place_move(self, **kwargs):
        piece = kwargs['piece']
        column = kwargs['arg']
        try:
            column = int(column)
        except ValueError:
            raise InvalidArgError()
        if not len(self.moves) and column == 3:
            raise IllegalFirstMoveError()  
        if column < 0 or column > 6:
            raise PlayOutOfBoundsError() 
        if self.board[0][column]:
            raise ColumnIsFullError()
        for row in range(0, 5):
            if self.board[row+1][column]:
                self.register_move(row, column, piece)
                return None
        self.register_move(5, column, piece)
        return None
    
    # Actually place the move in the double array and list.   
    def register_move(self, row, column, piece):
        self.board[row][column] = piece
        self.moves.append(Move(row, column, piece))
        
    # Careful! If called on an empty list it'll blow up.
    def get_coords_of_last_move(self):
        last_move_index = len(self.moves) - 1
        return self.moves[last_move_index].get_coordinates()
    
    def get_empty_columns(self, **kwargs):
        answer = ''
        for col in range(0, 7):
            if not self.board[5][col]:
                answer += str(col)
        return answer
    
    # Differs from get_coords because it returns a string and an
    # arbitrary number of turns past.
    def get_last_plays_position(self, **kwargs):
        num_of_plays = kwargs['arg']
        try:
            int(num_of_plays)
        except TypeError:
            raise InvalidArgError()
        answer = ''
        index = 0
        while index < num_of_plays and (index*2 + 1) <= len(self.moves):
            prev_move_index = len(self.moves) - (index*2 + 1)
            try:
                coords = self.moves[prev_move_index].get_coordinates()
            except IndexError:
                break
            else:
                answer += str(coords[0]) + str(coords[1])
            index += 1
        return answer
        
    # Really only for debugging...
    def print_to_console(self):
        for row in range(0, 6):
            for column in range(0, 7):
                sys.stdout.write( "|" + str(self.board[row][column]))
            print "|"
            
    def write_board_to_log(self):
        for row in range(0, 6):
            s = ''
            for column in range(0, 7):
                s += '|' + str(self.board[row][column])
            Logger().write(s + '|')
        Logger().write('\n')
           
    # Returns entire board as string, top to bottom, left to right.
    # See method __repr__ 
    def get_board(self, **kwargs):
        return repr(self)
                
    ''' 
    Everything below this is checking for wins. It feels shitty. 
    Suggestions for improvement welcome.
    '''
    def was_move_a_winner(self):
        try:
            row, column = self.get_coords_of_last_move()
        except IndexError:
            # No moves yet
            return False
        if (self.check_horizontal(row, column) 
            or self.check_vertical(row, column) 
            or self.check_hor_from_topleft(row, column) 
            or self.check_hor_from_topright(row, column)):
            return True
        return False
        
    def check_horizontal(self, row, column):
        while(column > 0 
              and self.board[row][column - 1] == self.board[row][column]):
            column -= 1
        count = 1
        while(column < 6 
              and self.board[row][column + 1] == self.board[row][column]):
            column += 1
            count += 1
        if count >= 4:
            return True
        return False
        
    def check_vertical(self, row, column):
        while(row > 0 
              and self.board[row - 1][column] == self.board[row][column]):
            row -= 1
        count = 1
        while(row < 5 
              and self.board[row + 1][column] == self.board[row][column]):
            row += 1
            count += 1
        if count >= 4:
            return True
        return False
    
    def check_hor_from_topleft(self, row, column):
        while(row > 0
              and column > 0 
              and self.board[row - 1][column - 1] == self.board[row][column]):
            row -= 1
            column -= 1
        count = 1
        while(row < 5
              and column < 6
              and self.board[row + 1][column + 1] == self.board[row][column]):
            row += 1
            column += 1
            count += 1
        if count >= 4:
            return True
        return False

    def check_hor_from_topright(self, row, column):
        while(row > 0
              and column < 6
              and self.board[row - 1][column + 1] == self.board[row][column]):
            row -= 1
            column += 1
        count = 1
        while(row < 5
              and column > 0
              and self.board[row + 1][column - 1] == self.board[row][column]):
            row += 1
            column -= 1
            count += 1
        if count >= 4:
            return True
        return False