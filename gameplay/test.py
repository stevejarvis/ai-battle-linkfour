'''
Created on Jan 25, 2012

@author: steve
'''

from utility import Utility
from board import Board
import socket
import threading
import time
import unittest

class Test(unittest.TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_whether_utility_is_singleton(self):
        util1 = Utility()
        util2 = Utility()
        assert(util1 is util2)
        util3 = Utility()
        assert(util1 is util3)

    def test_connections(self):
        self.make_two_new_players()
        Utility().start_listening()
        players = Utility().get_players()
        assert(players[0])
        assert(players[1])
        Utility().cleanup()
        
    def make_two_new_players(self):
        threading.Thread(group=None,
                         target=self.player_go).start()
        threading.Thread(group=None,
                         target=self.player_go).start()
        
    def player_go(self):
        time.sleep(1)   # Time for server to set up
        HOST = '127.0.0.1'
        PORT = Utility().get_port()
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.connect((HOST, PORT))
        sock.close()
        return
    
    def test_board_playing(self):
        board = Board()
        board.place_move(arg=0, piece='r')
        board.place_move(arg=0, piece='b')
        board.place_move(arg=5, piece='r')
        board.place_move(arg=6, piece='b')
        board.place_move(arg=6, piece='r')
        board.print_to_console()
        assert(board.get_board() == '----------------------------b-----rr----rb')
    
    def test_column_is_full(self):  
        board = Board()
        try:
            for row in range(0, 7):
                board.place_move(arg=1, piece='b')
        except Exception as e:
            assert(True)
        else:
            assert(False)
            
    def test_out_of_bounds_board(self):
        board = Board()
        try:
            board.place_move(arg=-1, piece='b')
        except Exception as e:
            assert(True)
        else:
            assert(False)
        try:
            board.place_move(7, 'b')
        except Exception as e:
            assert(True)
        else:
            assert(False)
            
    def test_winner_horizontal(self):
        board = Board()
        assert not board.check_horizontal(5, 2)
        for i in range(1, 4):
            board.place_move(arg=i, piece='r')
        assert not board.check_horizontal(5, 2)
        assert not board.check_horizontal(5, 1)
        board.place_move(arg=0, piece='b')
        assert not board.check_horizontal(5, 0)
        board.place_move(arg=4, piece='r')
        assert board.check_horizontal(5, 2)
        
    def test_winner_vertical(self):
        board = Board()
        for i in range(1, 4):
            board.place_move(arg=4, piece='r')
        assert not board.check_vertical(4, 3)
        assert not board.check_vertical(5, 3)
        assert not board.was_move_a_winner()
        board.place_move(arg=0, piece='b')
        assert not board.check_vertical(5, 0)
        board.place_move(arg=4, piece='r')
        assert board.check_vertical(5, 4)
        assert board.was_move_a_winner()
        
    def test_winner_tlbr(self):
        board = Board()
        for i in range(1, 5):
            board.place_move(arg=2, piece='b')
        for i in range(1, 4):
            board.place_move(arg=3, piece='b')
        for i in range(1, 3):
            board.place_move(arg=4, piece='b')
        for i in range(1, 2):
            board.place_move(arg=5, piece='b')
        assert board.check_hor_from_topleft(4, 4)
        assert not board.check_hor_from_topleft(5, 3)
        for i in range(2, 6):
            board.place_move(arg=i, piece='r')
        assert board.check_hor_from_topleft(4, 5)
        
    def test_winner_trbl(self):
        board = Board()
        for i in range(1, 2):
            assert not(board.place_move(arg=2, piece='b'))
        for i in range(1, 3):
            board.place_move(arg=3, piece='b')
        for i in range(1, 4):
            board.place_move(arg=4, piece='b')
        for i in range(1, 5):
            board.place_move(arg=5, piece='b')
        assert board.check_hor_from_topright(4, 3)
        assert not board.check_hor_from_topright(5, 3)
        for i in range(2, 6):
            board.place_move(arg=i, piece='r')
        assert board.check_hor_from_topright(1, 5)
        assert board.check_hor_from_topright(4, 2)
        assert board.check_hor_from_topright(3, 3)
        assert board.was_move_a_winner()
        
    def test_last_coords(self):
        board = Board()
        board.place_move(arg=5, piece='b')
        assert(board.get_coords_of_last_move() == (5,5))
        assert(board.get_last_plays_position(arg=1) == '55')
        board.place_move(arg=5, piece='b')
        assert(board.get_coords_of_last_move() == (4,5))
        assert(board.get_last_plays_position(arg=1) == '45')
        
    def test_empty_columns_check(self):
        board = Board()
        board.place_move(arg=5, piece='b')
        board.place_move(arg=2, piece='b')
        board.place_move(arg=1, piece='b')
        board.place_move(arg=5, piece='b')
        assert(board.get_empty_columns() == '0346')
        
    def test_this_crazy_last_plays(self):
        board = Board()
        board.place_move(arg=5, piece='b')
        board.place_move(arg=3, piece='b')
        board.place_move(arg=4, piece='b')
        board.place_move(arg=5, piece='b')
        board.place_move(arg=2, piece='b')
        board.place_move(arg=1, piece='b')
        board.place_move(arg=0, piece='b')
        board.place_move(arg=5, piece='b')
        board.place_move(arg=6, piece='b')
        board.place_move(arg=5, piece='b')
        # 5,5  5,3  5,4  4,5  5,2  5,1  5,0  3,5  5,6  2,5
        assert(board.get_last_plays_position(arg=1) == '25')
        assert(board.get_last_plays_position(arg=4) == '25355145')
        assert(board.get_last_plays_position(arg=49) == '2535514553')
        assert(board.get_last_plays_position(arg=50) == '2535514553')
        assert(board.get_last_plays_position(arg=-1) == '')
        try:
            board.get_last_plays_position(arg='stupid')
        except:
            assert True
        else:
            assert False
        
if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()