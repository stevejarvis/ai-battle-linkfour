'''
Created on Jan 4, 2012

@author: steve

Raise a new error for naughtiness.
'''

class PlayOutOfBoundsError(Exception):

    def __str__(self):
        return "1 Play was out of bounds of the board."  
    
class ColumnIsFullError(Exception):

    def __str__(self):
        return "2 That column is full."
    
class IllegalFirstMoveError(Exception):
    
    def __str__(self):
        return "3 First move can't be in center column."   
    
class CommandNotFoundError(Exception):
    
    def __str__(self):
        return "4 Command was not found. Invalid."
    
class InvalidArgError(Exception):
    
    def __str__(self):
        return "5 Argument is invalid."