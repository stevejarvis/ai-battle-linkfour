'''
Created on Jan 25, 2012

@author: steve

'''

import socket
from player import Player
import os
import time

class Logger(object):
    '''
    Writes the game play to a log.
    '''
    
    file_name = str(int(time.time()))
    
    def __new__(cls):
        try:
            return cls._instance
        except:
            cls._instance = super(Logger, cls).__new__(cls)
            return cls._instance        
        
    def write(self, line):
        if not os.path.exists('log'):
            os.makedirs('log')
        self.log_file_name = 'log/' + self.file_name + '.txt'
        #self.log_file_name = str(time.time()) + ".txt"
        # The 0 is the buffer size. So changes are written immediately.
        self.log_file = open(self.log_file_name,"a", 0)
        self.log_file.write(line + '\n')

class EmptySpace(object):
    '''
    Represents an unplayed space on the board. Allows it to
    always return unequal (useful for win checking reliability)
    and represent the empty spaces however we want.
    '''
    
    def __new__(cls):
        try:
            return cls._instance
        except:
            cls._instance = super(EmptySpace, cls).__new__(cls)
            return cls._instance
        
    def __nonzero__(self):
        return 0
    
    def __eq__(self, other):
        # Aint equal to nothin!
        return False
    
    def __repr__(self):
        return '-'

class Utility(object):
    '''
    Class handles things like opening/closing connections.
    Makes new players.
    '''
    
    def __new__(cls):
        try:
            return cls._instance
        except:
            cls._instance = super(Utility, cls).__new__(cls)
            return cls._instance

    def __init__(self):
        self.host = '127.0.0.1'
        self.port = 12345
        
    def start_listening(self):
        self.sock = socket.socket(socket.AF_INET,
                                    socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, 
                             socket.SO_REUSEADDR, 
                             1)
        self.sock.bind((self.host, self.port))
        self.sock.listen(2) # 2 connections before refusing
        
    def get_port(self):
        return self.port
        
    def get_players(self):
        self.red_conn, self.red_addr = self.sock.accept()
        self.black_conn, self.black_addr = self.sock.accept()
        return [Player(self.red_conn, 'r'), Player(self.black_conn, 'b')]
    
    def cleanup(self):
        self.sock.close()
