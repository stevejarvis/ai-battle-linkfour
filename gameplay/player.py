'''
Created on Jan 28, 2012

@author: steve

'''

class Player(object):
    '''
    Player class represents one of the connections. Handles the messaging
    and remembers the socket and team.
    '''
    
    def __init__(self, sock, team):
        self.socket = sock
        self.team = Piece(team)
        
    def get_sock(self):
        return self.socket
    
    def get_team(self):
        return str(self.team)
        
    def send_msg(self, msg):
        try:
            self.socket.send(msg + '\n')
        except Exception as e:
            print e
        
    def get_msg(self):
        try:
            return self.socket.recv(1024)
        except Exception as e:
            print e
            
class Piece(object):
    '''
    Piece class was created solely to print the little r and b letters
    without quotes.
    '''
    
    def __init__(self, team):
        self.team = team
        
    def __str__(self):
        return self.team
        #return self.team[1:2]