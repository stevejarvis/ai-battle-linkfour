'''
Created on Jan 28, 2012

@author: steve

Main deal starts the game and takes requests over the socket. 

See docs.txt.
'''

from utility import Utility
from error import *
from board import Board
import threading
import time
import os
from utility import Logger

def we_have_a_winner(player_id):
    message_player(player_id, '9 Winner')
    message_player((player_id + 1) % 2, '9 Loser')
    Logger().write('Player ' + str(player_id) + ', ' + players[player_id].get_team() + ', wins!')
    os._exit(0)  # Like kill -9, but if we already know the winner who cares
    
# Current player loses if this goes off. They took too long. Like a bomb.      
def took_too_long(arg):
    we_have_a_winner((arg + 1) % 2)
    pass
    
def message_player(player_id, msg):
    players[player_id].send_msg(msg)
    
def get_msg_from_player(player_id):
    return players[player_id].get_msg()

# Needs to be a 1:1 correspondence of questions and answers
def make_a_decision(player_id):
    while True:
        response_from_player = get_msg_from_player(player_id)
        try:
            comm = response_from_player.split(None, 1)
        except AttributeError as e:
            try:
                raise InvalidArgError
            except InvalidArgError as e:
                message_player(player_id, str(e))
                continue
        # Only make_move will have arg, the rest need to not crash. Ignore it.
        comm.append('')
        
        try:
            response_for_player = interfaces[comm[0]](arg=comm[1],
                                            id=player_id, 
                                            piece=players[player_id].get_team())
        except KeyError as e:
            try:
                raise CommandNotFoundError()
            except CommandNotFoundError as e:
                message_player(player_id, str(e))
                continue
        except PlayOutOfBoundsError as e:
            message_player(player_id, str(e))
            continue
        except ColumnIsFullError as e:
            message_player(player_id, str(e))
            continue
        except IllegalFirstMoveError as e:
            message_player(player_id, str(e))
            continue
        except InvalidArgError as e:
            message_player(player_id, str(e))
            continue
        else:
            if response_for_player:
                message_player(player_id, response_for_player)
            else:
                # He played successfully
                break;

board = Board()
Utility().start_listening()
players = Utility().get_players()

# The keys come from messages from socket, map to appropriate methods.
interfaces = {
        'get_last_plays_position':board.get_last_plays_position, # arg = number of plays
        'get_empty_columns':board.get_empty_columns,
        'make_move':board.place_move,   # arg = column number
        'get_board':board.get_board
    }

if __name__ == '__main__':
    turn_count = 2
    
    while not board.was_move_a_winner():
        time.sleep(1) # Just nice to watch what's happening.
        current_player = turn_count % 2
        
        message_player(current_player, ' 0 Get Move')
        timer = threading.Timer(10, took_too_long, [current_player])
        timer.start()
            
        make_a_decision(current_player)
            
        timer.cancel()
        turn_count += 1
        board.write_board_to_log()
        
            
    we_have_a_winner((turn_count + 1) % 2)