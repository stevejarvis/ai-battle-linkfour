/*
 * Middle man helper guy for java players. Pass team name
 * as arg. Filename and main class name must match team
 * name.
 */

import java.lang.reflect.*;
import java.net.*;
import java.io.*;

public class Midman {
	/*
	 * Passes messages from the player to the server, visa versa.
	 */

	private Socket sock = null;
	private BufferedReader sockIn = null;
	private PrintWriter sockOut = null;
	private Object player = null;
	private Method playerReceiveMessage = null;

	private Midman(Constructor con) {
		try {
			this.player = con.newInstance(this);
		} catch(Exception e) {
			System.err.println(e);
		}
		
		// Get it's magic method to receive messages
		try {
			Class<?> temp = this.player.getClass();
			this.playerReceiveMessage = temp.getMethod("receiveMessage", String.class);
		} catch(Exception e) {
			System.err.println(e);
		}
		
		String host = "127.0.0.1";
		int port = 12345;
		try { 
			this.sock = new Socket(host, port);
			this.sockIn = new BufferedReader(new InputStreamReader(sock.getInputStream()));
			this.sockOut = new PrintWriter(sock.getOutputStream(), true);
		} catch(Exception e){
			System.out.println("Is the server started?");
			System.exit(0);
		}
	}

	public void sendMessage(String msg) {
		try{
			this.sockOut.println(msg);
		} catch(Exception e) {
			System.err.println(e);
		}
	}
	
	private String getMessageFromServer() {
		String msg = "";
		try {
			msg = sockIn.readLine();
		} catch(Exception e) {
			System.err.println(e);
			cleanup();
		}
		return msg;
	}
	
	private void sendMessageToPlayer(String msg) {
		try {
			this.playerReceiveMessage.invoke(this.player, msg);
		} catch(Exception e) {
			System.err.println(e);
		}
	}
	
	private void cleanup() {
		try{
			this.sockIn.close();
			this.sockOut.close();
			this.sock.close();
		} catch(Exception e) {
			System.err.println(e);
		} finally {
			System.exit(0);
		}
	}
	
	private void go() {
		while(true) {
			String msg = getMessageFromServer();
			if(msg.charAt(0) == '9') {
				// Game over.
				cleanup();
			}
			sendMessageToPlayer(msg);
		}
	}

	public static void main(String[] args) {
		
		Class cls = null;
		Midman midman = null;
		
		try {
			cls = Class.forName(args[0]);
		} catch (Exception e) {
			System.out.println("Team name required as argument.");
			System.exit(0);
		}
		
		try {
            Constructor[] cons = cls.getConstructors();
			// Will be an issue if there are multiple constructors!
			midman = new Midman(cons[0]);
         } catch (Throwable e) {
            System.err.println(e);
			System.exit(0);
         }
		
		midman.go();
	}
}