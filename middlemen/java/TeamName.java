import java.util.Random;

public class TeamName {

	private Midman helper = null;
	
	public TeamName(Midman helper){
		this.helper = helper;
	}
	
	public void receiveMessage(String msg) {
		figureOutWhatToDo();
	}
	
	private void sendMove(String msg) {
		this.helper.sendMessage(msg);
	}
	
	private void figureOutWhatToDo() {
		Random rand = new Random();
		int num = rand.nextInt(7);
		String msg = "make_move " + num;
		System.out.println(msg);
		sendMove(msg);
	}
}
