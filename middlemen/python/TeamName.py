'''
Example player in python. The name of the player class must match your team name.
It is instantiated by the "helper" and an instance of the helper will be passed 
to the construcor. The only method that must be implemeneted is:
	
	receive_message(message):
		# Called by helper and passed a message from the server.

Make plays and ask questions by sending messages through the helper.

	helper.send_message(msg)
		# Send your response or question to the server via the helper.

See the site for more details: http://notmadeyet.com
'''

import random

class TeamName(object):
	'''
	Main object for player.
	'''

	def __init__(self, helper):
		self.helper = helper

	def receive_message(self, msg):
		message_from_server = msg	
		self.figure_out_what_to_do(msg)

	def send_play(self, msg):
		self.helper.send_message(msg)

	def figure_out_what_to_do(self, msg):
		message_for_server = 'make_move ' + str(random.randint(0,6))
		print message_for_server
		self.send_play(message_for_server)