import socket
import inspect
import sys
import imp

'''
Pass team name as arg. Main class name must match team name.
'''

class MiddleMan(object):
	'''
	The python middle man.

	Passes messages from the player to the server, visa versa.
	'''

	def __init__(self, obj):
		self.player = obj(self)
		self.host = '127.0.0.1'
		self.port = 12345
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		try:
			self.sock.connect((self.host, self.port))
		except:
			print 'Is server running?'
			sys.exit(0)
		self.__go()
	
	def __get_message_from_server(self):
		return self.sock.recv(1024)
	
	def send_message(self, msg):
		self.sock.send(msg)

	def __send_message_to_player(self, msg):
		self.player.receive_message(msg)

	def __cleanup(self):
		self.sock.close()
		sys.exit(0)
		
	def __go(self):
		while True:
			msg = self.__get_message_from_server()
			if msg[:1] == '9':
				# Game over.
				self.__cleanup()
			self.__send_message_to_player(msg)

if __name__ == '__main__':
	try:
		team_name = sys.argv[1]
	except IndexError:
		print 'Team name required as argument.'
		sys.exit(0)

	(file, path, desc) = imp.find_module(team_name)
	mod = imp.load_module(team_name, file, path ,desc)
	tclasses = inspect.getmembers(mod, inspect.isclass)
	for pair in tclasses:
		if pair[0] == team_name:
			obj = pair[1]

	midman = MiddleMan(obj)
