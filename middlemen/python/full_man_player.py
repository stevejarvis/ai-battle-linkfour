import socket
import random
import time
import sys

class Player(object):
	'''
	A complete player in Python.
	Requires manual moves to play. Not the format
	for a player in the contest. For debug only!

	'''

	def __init__(self):
		self.host = '127.0.0.1'
		self.port = 12345
		self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		try:
			self.sock.connect((self.host, self.port))
		except:
			print "Is the server started?"
			sys.exit(0)
	
	def get_message_from_server(self):
		self.msg = self.sock.recv(1024)
		print "Got message: " + str(self.msg)
	
	def send(self, move):
		print "Sending move: " + str(move)
		try:
			self.sock.send(str(move))
		except:
			# Game over
			sys.exit(0)

	def cleanup(self):
		self.sock.close()
		sys.exit(0)

if __name__ == '__main__':
	play = Player()
	while True:
		try:
			play.get_message_from_server()
		except:
			# Game over
			play.cleanup()
		else:
			play.send(raw_input("DO: "))
